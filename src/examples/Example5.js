import React, { Fragment } from 'react';
import AutoCompleteText from '../components/AutoCompleteText';

function template(todo) {
  return (
    <table width="100%" border="0">
      <tbody>
        <tr>
          <td>
            <strong>{todo.title}</strong> 
          </td>
          <td width="25px" align="center">
            { todo.completed ? <span>&#10004;</span> : <span>&#10008;</span> }
          </td>
        </tr>
      </tbody>
    </table>
  );
}

function templateSelected(todo) {
  return (
    <strong>{ todo.title }</strong>
  );
}

const Example5 = () => {
  return (
    <Fragment>
      <h3>Todos Ajax</h3>
      <AutoCompleteText
        name="test"
        placeholder="Search"
        width="286px"
        ajax={{
          url: 'https://jsonplaceholder.typicode.com/todos',
          dataType: 'json',
          results: (data, term) => {
            const regex = new RegExp(`${term}`, 'i');
            let suggestions = data.filter(item => regex.test(item.title));

            return suggestions;
          }
        }}
        allowClear={ true }
        suggestionTemplate={ template }
        selectedSuggestionTemplate={ templateSelected }
      />
    </Fragment>
  );
};

export default Example5;