import React, { Fragment } from 'react';
import AutoCompleteText from '../components/AutoCompleteText';

function template(repositorie) {
  return (
    <table width="100%" border="0" cellPadding="2">
      <tbody>
        <tr>
          <td>
            <img src={ repositorie.owner.avatar_url } width="50px" alt="" />
          </td>
          <td>
            <div style={ {fontSize: "15px", fontWeight: 'bold', textAlign: 'center'} }>{ repositorie.full_name }</div>
            <p style={ {color: '#ddd', fontSize: "10px", fontStyle: 'italic'} }>
              { repositorie.description }
            </p>
          </td>
        </tr>
      </tbody>
    </table>
  );
}

function templateSelected(repositorie) {
  return repositorie.full_name;
}

const Example6 = () => {
  return (
    <Fragment>
      <h3>Github repositories</h3>
      <AutoCompleteText
        name="repositorie"
        placeholder="Search"
        ajax={{
          url: 'https://api.github.com/search/repositories',
          dataType: 'json',
          data: (term) => {
            return {
              q: term,
            };
          },
          results: (data, term) => {
            return data.items;
          }
        }}
        allowClear={ true }
        suggestionTemplate={ template }
        selectedSuggestionTemplate={ templateSelected }
      />
    </Fragment>
  );
};

export default Example6;