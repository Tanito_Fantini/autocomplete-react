import React, { Fragment } from 'react';
import AutoCompleteText from '../components/AutoCompleteText';

const colours = ['silver', 'gray', 'black', 'red', 'maroon', 'yellow', 'olive', 'lime', 'green', 'aqua', 'teal', 'blue', 'navy', 'fuchsia', 'purple'];

function template(value) {
  return (
    <table width="100%" border="0" cellPadding="5" cellSpacing="0">
      <tbody>
        <tr>
          <td>
            <strong>{ value.charAt(0).toUpperCase() + value.slice(1) }</strong>
          </td>
          <td width="25%">
            <div style={ {backgroundColor: value} }>&nbsp;</div>
          </td>
        </tr>
      </tbody>
    </table>
  );
}

const Example1 = () => {
  return (
    <Fragment>
      <h3>Colours</h3>
      <AutoCompleteText
        name="colours"
        placeholder="Select a colour"
        data={ colours.sort() }
        suggestionTemplate={ template }
      />
    </Fragment>
  );
};

export default Example1;