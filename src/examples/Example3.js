import React, { Fragment, Component } from 'react';
import AutoCompleteText from '../components/AutoCompleteText';

class Example3 extends Component {

  constructor(props) {
    super(props);

    this.state = {
      disabled: true
    };
  }

  change = () => {
    this.setState({
      disabled: !this.state.disabled
    });
  }

  render() {
    return (
      <Fragment>
        <h3>Enabled/Disabled</h3>
        <button onClick={ this.change } disabled={ !this.state.disabled }>Enabled</button>
        &nbsp;
        <button onClick={ this.change } disabled={ this.state.disabled }>Disabled</button>

        <hr />

        <AutoCompleteText
          name="exmaple3"
          value={ {id: 1, text: this.state.disabled ? "It's disabled" : "It's enabled"} }
          disabled={ this.state.disabled }
          data={ [] }
        />
      </Fragment>
    );
  }
}

export default Example3;