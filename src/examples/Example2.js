import React, { Fragment } from 'react';
import AutoCompleteText from '../components/AutoCompleteText';
import countries from '../countries';

function template(value) {
  return (
    <div><strong>Country:</strong> {value}</div>
  );
}

function templateSelected(value) {
  console.log( value );
  return (
    <strong>! { 'value' } !</strong>
  );
}

const Example2 = () => {
  return (
    <Fragment>
      <h3>Countries</h3>
      <AutoCompleteText
        name="test"
        placeholder="Search"
        // readonly={ false }
        data={ countries }
        maxlength={ 2 }
        allowClear={ true }
        suggestionTemplate={ template }
        selectedSuggestionTemplate={ templateSelected }
      />
    </Fragment>
  );
};

export default Example2;