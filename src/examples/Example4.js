import React, { Component, Fragment } from 'react';
import AutoCompleteText from '../components/AutoCompleteText';

const data = [
  {id: 1, text: 'Goku'},
  {id: 2, text: 'Bulma'},
  {id: 3, text: 'Krillin'},
  {id: 4, text: 'Vegeta'},
  {id: 5, text: 'Trunks'},
  {id: 6, text: 'Yamcha'},
];

class Example4 extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      disabled: true
    };

    this.child = React.createRef();
  }

  open = () => {
    this.child.current.open();
  }

  close = () => {
    this.child.current.close();
  }
  
  setGoku = () => {
    this.child.current.suggestionSelected(data[0]);
  }
  
  render() {
    return (
      <Fragment>
        <h3>Dragon Ball</h3>
        <button onClick={ this.open }>Open</button>
        &nbsp;
        <button onClick={ this.close }>Close</button>
        &nbsp;
        <button onClick={ this.setGoku }>Set Goku</button>
  
        <hr />
  
        <AutoCompleteText
          ref={ this.child }
          name="exmaple4"
          value={ data[0] }
          data={ data }
        />
      </Fragment>
    );
  }
}

export default Example4;