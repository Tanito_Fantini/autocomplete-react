import React, { Fragment } from 'react';
import AutoCompleteText from '../components/AutoCompleteText';

const clubs = [
  {id: 1, text: 'Boca', disabled: true},
  {id: 1, text: 'Manchester United'},
  {id: 1, text: 'Real Madrid'},
  {id: 1, text: 'Chelsea', disabled: true},
  {id: 1, text: 'River'},
  {id: 1, text: 'Liverpool'},
  {id: 1, text: 'Manchester City'},
  {id: 1, text: 'Inter', disabled: true},
  {id: 1, text: 'Juventus'},
  {id: 1, text: 'PSG'},
];

const Example7 = () => {
  return (
    <Fragment>
      <h3>Football clubs</h3>
      <AutoCompleteText
        name="example7"
        placeholder="Search"
        data={ clubs.sort() }
      />
    </Fragment>
  );
};

export default Example7;