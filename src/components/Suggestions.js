import React, { Fragment } from 'react';

const Suggestions = ({
  show,
  inputValue,
  inputRef,
  onChangeInput,
  onKeyDownInput,
  onBlurInput,
  searching,
  renderedSuggestions
}) => {
  const class_input_search = (show ? '' : 'hidden ') + (searching ? 'searching' : 'search');
  
  return (
    <Fragment>
    {
      show &&
        <div className="container-suggestions">
        <input
          type="text"
          value={ inputValue }
          ref={ inputRef }
          className={ class_input_search }
          onChange={ onChangeInput }
          onKeyDown={ onKeyDownInput }
          onBlur={ onBlurInput }
        />

        { renderedSuggestions }
      </div>
    }
    </Fragment>
  );
};

export default Suggestions;