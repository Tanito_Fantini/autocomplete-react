import React, { Component, Fragment } from 'react';
import './AutoCompleteText.scss';
import SuggestionsSelected from './SuggestionsSelected';
import Suggestions from './Suggestions';
import Mask from './Mask';
import PropTypes from 'prop-types';
import axios from 'axios';

class AutoCompleteText extends Component {

  constructor(props) {
    super(props);

    this.DEBUG = true;

    // references
    this.container    = React.createRef();
    this.input        = React.createRef();
    this.input_search = React.createRef();

    this.timeout = null;
    // const CancelToken = axios.CancelToken;
    this.cancelAjax = null;

    this.state = {
      focus: false,
      open: false,
      suggestion_selected: '',
      input_text: '',
      active: -1,
      suggestions: [],
      searching: false
    };

    this.onFocusInput          = this.onFocusInput.bind(this);
    this.onBlurInput           = this.onBlurInput.bind(this);
    this.onKeyDownInput        = this.onKeyDownInput.bind(this);
    this.open                  = this.open.bind(this);
    this.close                 = this.close.bind(this);
    this.onTextChanged         = this.onTextChanged.bind(this);
    this.search                = this.search.bind(this);
    this.onKeyDown             = this.onKeyDown.bind(this);
    this.suggestionSelected    = this.suggestionSelected.bind(this);
    this.onMouseEnter          = this.onMouseEnter.bind(this);
    this.getSuggestionSelected = this.getSuggestionSelected.bind(this);
    this.renderSuggestions     = this.renderSuggestions.bind(this);
    this.clear                 = this.clear.bind(this);
    this.onBlurInputSearch     = this.onBlurInputSearch.bind(this);
  }

  // it is invoked immediately after a component is mounted
  componentDidMount() {
    const { value } = this.props;

    if( value !== null ) {
      this.suggestionSelected(value);
    }
  }

  // it is invoked immediately after updating occurs
  componentDidUpdate(prevProps, prevState, snapshot) {
    let updateValue = false;

    if( this.props.value !== null ) {
      if( typeof(prevProps.value) !== typeof(this.props.value) ) {
        updateValue = true;
      }
      else {
        switch( typeof(this.props.value) ) {
          case 'string':
            if( prevProps.value !==  this.props.value) {
              updateValue = true;
            }
            break;
  
          case 'object':
            if( prevProps.value.id !==  this.props.value.id) {
              updateValue = true;
            }
            break;
  
            // no default
        }
      }
    }

    if( updateValue ) {
      this.suggestionSelected(this.props.value);
    }
  }

  componentWillUnmount() {
    // cancel the search request
    if( this.timeout ) {
      clearTimeout(this.timeout);
    }

    // cancel the ajax request
    if( this.cancelAjax !== null ) {
      this.cancelAjax();
    }
  }

  onFocusInput() {
    if( this.DEBUG )
      console.log('onFocusInput');

    this.setState({
      focus: true
    });
  }

  onBlurInput() {
    if( this.DEBUG )
      console.log('onBlurInput');

    this.setState({
      focus: false
    });
  }

  onKeyDownInput(event) {
    // console.log( event.keyCode );

    switch( event.keyCode ) {
      case 13:
        // enter
        // no-fallthrough
      case 38:
        // up arrow
        // no-fallthrough
      case 40:
        // down arrow
        this.open();
        break;

      // no default
    }
  }

  getIndexFirstActive(data = this.state.suggestions) {
    let index = -1;

    outer_block:
      for( let i=0, length=data.length; i<length; i++ ) {
        switch( typeof(data[i]) ) {
          case 'string':
            index = i;
            break outer_block;
  
          case 'object':
            if( !data[i].disabled ) {
              index = i;
              break outer_block;
            }

          // no default
        }
      }

    return index;
  }

  open() {
    const { readonly, disabled } = this.props;

    if( this.DEBUG )
      console.log('open');

    if( !readonly && !disabled && !this.state.open ) {
      this.setState({
        input_text: '',
        open: true,
        active: this.getIndexFirstActive(this.props.data),
        suggestions: this.props.data,
        searching: false
      }, () => {
        this.input_search.current.focus();
      });
    }
  }

  close(focus = false) {
    if( this.DEBUG )
      console.log('close');

    if( this.cancelAjax !== null ) {
      this.cancelAjax();
    }

    this.setState({
      open: false,
      active: -1
    }, () => {
      if( focus ) {
        this.input.current.focus();
      }
    });
  }

  onTextChanged(event) {
    const { value } = event.target;
    const { maximumInputLength, minimumInputLength, data, delay } = this.props;

    // cancel the search request if user types within the timeout
    if( this.timeout ) {
      clearTimeout(this.timeout);
    }

    if( value.length > maximumInputLength ) {
      this.setState({
        searching: false
      });

      return;
    }

    this.setState({
      input_text: value,
      active: -1,
      searching: true
    }, () => {
      if( value.length === 0 || value.length < minimumInputLength ) {
        if( this.cancelAjax !== null ) {
          this.cancelAjax();
        }

        this.setState({
          searching: false,
          active: this.getIndexFirstActive(data),
          suggestions: data,
        });
      }
      else {
        this.timeout = setTimeout(() => {
          this.search(value);
        }, delay);
      }
    });
  }

  search(text) {
    const { data, ajax } = this.props;

    if( data.length ) {
      const regex = new RegExp(`${text}`, 'i');
      let suggestions = data.filter(item => {
        var text = '';

        switch( typeof(item) ) {
          case 'string':
            text = item;
            break;

          case 'object':
            text = item.text;
            break;

          // no default
        }

        return regex.test(text);
      });

      if( this.props.maxlength > 0 && suggestions.length > this.props.maxlength ) {
        suggestions.length = this.props.maxlength;
      }

      this.setState({
        active: this.getIndexFirstActive(suggestions),
        suggestions,
        searching: false
      });
    }
    else
      if( Object.keys(ajax).length !== 0 ) {
        if( this.cancelAjax !== null ) {
          this.cancelAjax();
        }

        // send the request
        axios({
          url: ajax.url,
          method: ajax.method ? ajax.method : 'get',
          params: ajax.data ? ajax.data(text) : {},
          timeout: ajax.timeout ? ajax.timeout : 5000,
          responseType: ajax.dataType ? ajax.dataType : 'json',
          responseEncoding: 'utf8',
          transformResponse: ajax.results ? (data) => ajax.results(data, text) : null,
          cancelToken: new axios.CancelToken((c) => {
            // An executor function receives a cancel function as a parameter
            this.cancelAjax = c;
          }),
        })
        .then((response) => {
          if( this.DEBUG )
            console.log( response );

          this.setState({
            active: response.data.length ? 0 : -1,
            suggestions: response.data,
            searching: false
          });
        })
        .catch((error) => {
          if( error.response ) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
          }
          else
            if( error.request ) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log(error.request);
            }
            else {
              // Something happened in setting up the request that triggered an Error
              console.log('Error', error.message);
            }

          if( axios.isCancel(error) ) {
            console.log('Request canceled', error.message);
          }

          console.log(error.config);
        });
      }
  }

  onKeyDown(event) {
    const { active, suggestions } = this.state;
    let new_active = active;

    switch( event.keyCode ) {
      // enter
      case 13:
        if( active !== -1 ) {
          this.suggestionSelected(suggestions[active], true);
        }
        break;

      // key up
      case 38:
        event.preventDefault();

        if( active === 0 || active === -1 ) {
          return;
        }

        outer_block:
          for( let i=active-1; i >= 0; i-- ) {
            switch( typeof(suggestions[i]) ) {
              case 'string':
                new_active = i;
                break outer_block;

              case 'object':
                if( !suggestions[i].disabled ) {
                  new_active = i;
                  break outer_block;
                }

              // no default
            }
          }
  
        this.setState({
          active: new_active
        }, () => {
          let ul = document.getElementsByClassName('suggestions');

          if( ul.length ) {
            let li = ul[0].getElementsByClassName('active');

            if( li.length ) {
              li[0].scrollIntoView();
            }
          }
        });
        break;

      // key down
      case 40:
        event.preventDefault();

        if( active + 1 === suggestions.length || active === -1 ) {
          return;
        }

        outer_block:
          for( let i=active+1, length=suggestions.length; i < length; i++ ) {
            switch( typeof(suggestions[i]) ) {
              case 'string':
                new_active = i;
                break outer_block;

              case 'object':
                if( !suggestions[i].disabled ) {
                  new_active = i;
                  break outer_block;
                }

              // no default
            }
          }

        this.setState({
          active: new_active
        }, () => {
          let ul = document.getElementsByClassName('suggestions');

          if( ul.length ) {
            let li = ul[0].getElementsByClassName('active');

            if( li.length ) {
              li[0].scrollIntoView();
            }
          }
        });
        break;

      // esc
      case 27:
        this.close(true);
        break;

      // no default
    }
  };

  suggestionSelected(value, focus = false) {
    if( this.DEBUG )
      console.log('suggestionSelected', value);

    if( value !== null ) {
      this.setState({
        suggestion_selected: value,
        input_text: '',
      }, () => {
        if( focus ) {
          this.input.current.focus();
        }

        if( this.state.open ) {
          this.close();
        }
      });
    }
  }

  onMouseEnter(index) {
    if( !this.state.suggestions[index].disabled ) {
      this.setState({
        active: index
      });
    }
  }

  getSuggestionSelected() {
    const { suggestion_selected } = this.state;
    const suggestion = {
      id: '',
      html: ''
    };

    switch( typeof(suggestion_selected) ) {
      case 'string':
        if( suggestion_selected !== '' && this.props.selectedSuggestionTemplate ) {
          suggestion.id = suggestion_selected;
          suggestion.html = this.props.selectedSuggestionTemplate(suggestion_selected, this.container.current);
        }
        else {
          suggestion.id = suggestion_selected;
          suggestion.html = suggestion_selected;
        }
        break;

      case 'object':
        if( Object.keys(suggestion_selected).length !== 0 ) {
          suggestion.id = suggestion_selected.id;
          
          if( this.props.selectedSuggestionTemplate ) {
            suggestion.html = this.props.selectedSuggestionTemplate(suggestion_selected, this.container.current);
          }
          else {
            suggestion.html = suggestion_selected.text;
          }
        }
        break;

      // no default
    }

    return suggestion;
  }

  renderSuggestions() {
    const { suggestions, active, input_text, searching } = this.state;
    let suggestions_html = null;

    if( suggestions.length ) {
      suggestions_html = suggestions.map((item, index) => {
        let
            text      = '',
            className = 'suggestion',
            disabled  = false;

        switch( typeof(item) ) {
          case 'string':
            text = item;
            break;

          case 'object':
            text = item.text;

            if( item.disabled ) {
              disabled = true;
            }
            break;

          // no default
        }

        // Flag the active suggestion with a class
        if( index === active ) {
          className += ' active';
        }

        if( disabled ) {
          className += ' disabled';
        }

        return (
          <li
            className={ className }
            onClick={(event) => {
              event.stopPropagation();

              if( !disabled ) {
                this.suggestionSelected(item, true);
              }
            }}
            onMouseEnter={() => this.onMouseEnter(index)}
            key={ index }
          >
            { this.props.suggestionTemplate ? this.props.suggestionTemplate(item) : text }
          </li>
        );
      });
    }
    else
      if( input_text !== '' && !searching ) {
        suggestions_html = <li>No se encontraron resultados</li>;
      }
      else {
        return;
      }

    return(
      <ul className="suggestions" style={ {maxHeight: this.props.maxHeight} }>
        { suggestions_html }
      </ul>
    );
  }

  clear(event) {
    event.stopPropagation();

    this.setState({
      suggestion_selected: ''
    });
  }

  onBlurInputSearch() {
    // if( this.DEBUG )
    //   console.log('onBlurInputSearch');

    // setTimeout(() => {
    //   this.close();
    // }, 150);
  }

  render() {
    const { focus, input_text, open, searching } = this.state;
    const { width, readonly, disabled } = this.props;
    const suggestion = this.getSuggestionSelected();

    var class_container = 'container-autocomplete';

    if( focus ) {
      class_container += ' focus';
    }
    else
      if( readonly || disabled ) {
        class_container += ' disabled';
      }

    return (
      <Fragment>
        <div ref={ this.container } className={ class_container } onClick={ this.open } style={ {width: width} }>
          <input
            ref={ this.input }
            name={ this.props.name }
            value={ suggestion.id }
            readOnly={ true }
            onFocus={ this.onFocusInput }
            onBlur={ this.onBlurInput }
            onKeyDown={ this.onKeyDownInput }
          />

          <SuggestionsSelected
            showSuggestions={ open }
            suggestion={ suggestion }
            placeholder={ this.props.placeholder }
            allowClear={ this.props.allowClear }
            onClickClear={ this.clear }
          />

          <Suggestions
            show={ open }
            inputValue={ input_text }
            inputRef={ this.input_search }
            onChangeInput={ this.onTextChanged }
            onKeyDownInput={ this.onKeyDown }
            onBlurInput={ this.onBlurInputSearch }
            searching={ searching }
            renderedSuggestions={ this.renderSuggestions() }
          />
        </div>

        <Mask
          show={ open }
          onClick={ this.close }
        />
      </Fragment>
    );
  }

}

AutoCompleteText.defaultProps = {
  width: '100%',
  maxHeight: '150px',
  readonly: false,
  disabled: false,
  name: '',
  value: null,
  placeholder: '',
  allowClear: false,
  data: [],
  ajax: {},
  minimumInputLength: 1,
  maximumInputLength: 200,
  maxlength: 10,
  delay: 100,
  suggestionTemplate: null,
  selectedSuggestionTemplate: null,
}

AutoCompleteText.propTypes  = {
  width: PropTypes.string,
  maxHeight: PropTypes.string,
  readonly: PropTypes.bool,
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  placeholder: PropTypes.string,
  allowClear: PropTypes.bool,
  data: PropTypes.array,
  ajax: PropTypes.object,
  minimumInputLength: PropTypes.number,
  maximumInputLength: PropTypes.number,
  maxlength: PropTypes.number,
  delay: PropTypes.number,
  suggestionTemplate: PropTypes.func,
  selectedSuggestionTemplate: PropTypes.func,
};

export default AutoCompleteText;