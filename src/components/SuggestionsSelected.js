import React from 'react';

const SuggestionsSelected = ({
  showSuggestions,
  suggestion,
  placeholder,
  allowClear,
  onClickClear
}) => {
  return (
    <div className="suggestion-selected">
      {
        suggestion.html !== '' ?
          <div className='text'>{ suggestion.html }</div>
          :
          <div className='placeholder'>{ placeholder }</div>
      }
      {
        allowClear && !showSuggestions && suggestion.html !== '' && <span className="close" onClick={onClickClear}></span>
      }
      <span className="arrow" style={ { backgroundPosition: showSuggestions ? '-19px 3px' : '0 3px' } }></span>
    </div>
  );
};

export default SuggestionsSelected;