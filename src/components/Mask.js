import React, { Fragment } from 'react';

const Mask = ({onClick, show}) => {
  return (
    <Fragment>
      <div className="mask-autocomplete" onClick={ onClick } style={ { display: show ? 'block' : 'none' } }></div>
    </Fragment>
  );
};

export default Mask;