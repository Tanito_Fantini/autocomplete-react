import React from 'react';
import './App.css';

import Example1 from './examples/Example1';
import Example2 from './examples/Example2';
import Example3 from './examples/Example3';
import Example4 from './examples/Example4';
import Example5 from './examples/Example5';
import Example6 from './examples/Example6';
import Example7 from './examples/Example7';

function App() {
  return (
    <div className="App">
      <p>
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
      </p>
      <table width="600px" align="center">
        <tbody>
          <tr>
            <td>
              <h3>Nothing</h3>
              <input type="text" autoFocus={true} />
            </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellPadding="5">
                <tbody>
                  <tr>
                    <td width="50%">
                      <Example1 />
                    </td>
                    <td>
                      <Example2 />
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellPadding="5">
                <tbody>
                  <tr>
                    <td width="50%">
                      <Example3 />
                    </td>
                    <td>
                      <Example4 />
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellPadding="5">
                <tbody>
                  <tr>
                    <td width="50%">
                      <Example5 />
                    </td>
                    <td>
                      <Example6 />
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellPadding="5">
                <tbody>
                  <tr>
                    <td width="50%">
                      <Example7 />
                    </td>
                    <td>
                      
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <h3>Nothing</h3>
              <input type="text" />
            </td>
          </tr>
        </tbody>
      </table>
      <p>
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
      </p>
    </div>
  );
}

export default App;
